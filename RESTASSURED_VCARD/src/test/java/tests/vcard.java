package tests;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Test;


import org.apache.commons.io.FileUtils;
import org.hamcrest.Matcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.CoreConnectionPNames;

import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
@Listeners(tests.TestListeners.class)
public class vcard {
	
	
	public static WebDriver driver;	
	String timestamp = new SimpleDateFormat("MM.dd.yyyy.HH.mm.ss").format(new Date());
	public static Properties prop;	
	public static ExtentReports reports;
	public static ExtentTest test;	
	public static String baseuri="http://192.168.2.175/api/v1/json/";
	public static String FileName;
	public static ExcelNew TestData;
	public static String headerdate;
	public static String bearerToken;
public static String vCardCustomerCode;
public static String lenderAppNumber;
public static String lenderAccountNumber;
public static String lenderRelationshipNumber;
public static String idNumber;
public static String dateOfBirth;
public static String  phoneNbr;
public static String  emailId;
public static String ifscCode;
public static String AccountNumberTAG;
public static int loannumber;
public static String  netMonthlyIncome;
public static String authorizationCode;
	@BeforeMethod(alwaysRun = true)

	public void vCardSignIn() throws Exception{
		
		
		test = reports.startTest("vCardSignIn",	"API TESTING");
		
		
		String payload="{}";
		String uri=baseuri+"vCardSignIn";
		
		
		FileName = "scenario1.xls";
		String folder= System.getProperty("user.dir")+ prop.getProperty("Testdata_path");
		TestData = new ExcelNew(System.getProperty("user.dir")+ prop.getProperty("Testdata_path") + FileName);
		String TestData1= System.getProperty("user.dir")+ prop.getProperty("Testdata_path") + FileName;
		
	String sheetName = "Start";
	int lastrow = TestData.getLastRow("Start");
	
	for (int row = 2; row <= lastrow; row++) {

		String RunFlag = TestData.getCellData(sheetName, "Run", row);

		if (RunFlag.equals("Y")) {
			String user = TestData.getCellData(sheetName, "user", row);

			String pswd = TestData.getCellData(sheetName, "pswd", row);
			System.out.println(user+" is user");
		
	
			
			
			
			test.log(LogStatus.INFO, "Input as body= "+payload);	
			test.log(LogStatus.INFO, "Input as basicAuth user= "+user);	
			test.log(LogStatus.INFO, "Input as basicAuth password= "+pswd);	
			test.log(LogStatus.INFO, "Input as uri= "+uri);	
			
			Response res=
			given().auth().preemptive().basic(user,pswd).header("Content-type", "application/json")
			.body(payload)
			.when().post(uri).then().extract().response();
			
		
	/*		Response res1=
					given().auth().preemptive().basic(user,pswd).header("Content-type", "application/json")
					.body(b)
					.when().post(uri);	
System.out.println(res1);
			Thread.sleep(5000);
			Response res=res1.then().extract().response();
			System.out.println(res);	
			*/
			int  code=res.statusCode();
			String i=String.valueOf(code);
			if(i.equals("200")){
				 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>SIGIN PASS</b>");
					
			}else{
				 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>SIGIN FAIL DUE TO STATUS CODE</b>"+i);
					
				Assert.assertTrue(false);
				
			}
		
			bearerToken=res.header("Authorization");
			int s1=res.statusCode();
			System.out.println(s1+"Response is "+res.asString());

		//	test.log(LogStatus.PASS, "Response   status code and status " +res.statusCode()+" "+res.asString());
			//test.log(LogStatus.PASS, "Response  body is " +res.body().asString());
		//   	test.log(LogStatus.PASS, "Response header "+ res.headers());
			test.log(LogStatus.PASS, "Response header in Authorization Token "+ res.header("Authorization"));
		//	test.log(LogStatus.PASS, "Response header in date    "+ res.header("date"));

			test.log(LogStatus.INFO, "<FONT color=green>  TESTED "+uri);
			test.log(LogStatus.INFO, "******************************************************** ");

		  

		
		
		}
	}
	
			        
	}


	






	@Test(priority=01)
	public void Secnarios_API() throws Exception{
		
	
		
		
		test = reports.startTest("ALL APIS",	"API TESTING ");
		FileName = "getEncryptedValue_addCustomerInfo.xls";			
		String sheetName = "secanrio1";
		int lastrow = TestData.getLastRow("secanrio1");
		TestData = new ExcelNew(System.getProperty("user.dir")+ prop.getProperty("Testdata_path") + FileName);
		
		for (int row = 2; row <= lastrow; row++) {

			String RunFlag = TestData.getCellData(sheetName, "Run", row);
			
			if (RunFlag.equals("Y")) {				
				//String bearerToken = TestData.getCellData(sheetName, "Token", row);
				
				
			
				System.out.println(row);
				
				
				
	Response res1 = GetEncryptedValue.getEncryptedValue(row, bearerToken);
				GetEncryptedValue.getEncryptedValue_validate(res1);	
			
				
				/*	Response res2 = AddCustomerInfo.addCustomerInfo(row, bearerToken);
				AddCustomerInfo.addCustomerInfo_validate(res2);
				Response res3 = AddBankInfo.addBankInfo(row, bearerToken);///////////no
				AddBankInfo.addBankInfo_validate(res3);	
					Response res6 = OriginateLoan.originateLoan(row, bearerToken);
				OriginateLoan.originateLoan_validate(res6);
				Response res7 = ActivateAccount.activateAccount(row, bearerToken);
				ActivateAccount.activateAccount_validate(res7);
				
					Response res8=ViewCustomerProfile.viewCustomerProfile(row,bearerToken);///
				ViewCustomerProfile.viewCustomerProfile_validate(res8);
					Response res5 = EMandateCustomerRequest.eMandateCustomerRequest(row, bearerToken);//
				EMandateCustomerRequest.eMandateCustomerRequest_validate(res5);
				Response res12 = LoanAccountInquiry.LoanAccountInquiry(row, bearerToken);
				LoanAccountInquiry.LoanAccountInquiry_validate(res12);		
				Response res9 = ApproveDraw.approveDraw(row, bearerToken);
				ApproveDraw.approveDraw_validate(res9);	
				
				Response res10 = Draw.draw(row, bearerToken);
				Draw.draw_validate(res10);
				
				Response res15 = MakePaymentForvCard.makePaymentForvCard(row, bearerToken);
				MakePaymentForvCard.makePaymentForvCard_validate(res15);*/
				
				Response res21 = AddPtpAmountDetails.addPtpAmountDetails(row, bearerToken);
				AddPtpAmountDetails.addPtpAmountDetails_validate(res21);
/*
 		//emandatory status				
						
				

			Response res32 = CloseAccount.closeAccount(row, bearerToken);
				CloseAccount.closeAccount_validate(res32);		
	Response res31 = SuspendAccount.suspendAccount(row, bearerToken);
				SuspendAccount.suspendAccount_validate(res31);	
				


							
				Response res4 = GetDecryptedValue.getDecryptedValue(row, bearerToken);
				GetDecryptedValue.getDecryptedValue_validate(res4);	
			

				
				Response res13 = AddInvoice.addInvoice(row, bearerToken);
				AddInvoice.addInvoice_validate(res13);
				Response res14 = GetInvoice.getInvoice(row, bearerToken);
				GetInvoice.getInvoice_validate(res14);
			
				
		

				Response res16 = EmiConversion.emiConversion(row, bearerToken);
				EmiConversion.emiConversion_validate(res16);
				Response res17 = GetEmiPlans.getEmiPlans(row, bearerToken);
				GetEmiPlans.getEmiPlans_validate(res17);

				Response res18 = LoanTransactionsByStatement.loanTransactionsByStatement(row, bearerToken);
				LoanTransactionsByStatement.loanTransactionsByStatement_validate(res18);

				Response res19 = SendStatement.sendStatement(row, bearerToken);
				SendStatement.sendStatement_validate(res19);

				Response res20 = EmailStatement.emailStatement(row, bearerToken);
				EmailStatement.emailStatement_validate(res20);

				
				Response res22 = PayOffAmount.payOffAmount(row, bearerToken);
				PayOffAmount.payOffAmount_validate(res22);

				Response res23 = LoanTransactions.loanTransactions(row, bearerToken);
				LoanTransactions.loanTransactions_validate(res23);
				Response res24 = LoanPayments.loanPayments(row, bearerToken);
				LoanPayments.loanPayments_validate(res24);
				Response res29 = GetLoanDashBoardDetails.getLoanDashBoardDetails(row, bearerToken);
				GetLoanDashBoardDetails.getLoanDashBoardDetails_validate(res29);

				Response res30 = GetTransactionDetails.getTransactionDetails(row, bearerToken);
				GetTransactionDetails.getTransactionDetails_validate(res30);
				
				
				
				
				
				
				
				
				Response res33 = ListStatements.listStatements(row, bearerToken);
				ListStatements.listStatements_validate(res33);
			
				Response res34 = EmandateRegistrationBounce.emandateRegistrationBounce(row, bearerToken);
				EmandateRegistrationBounce.emandateRegistrationBounce_validate(res34);

				Response res35 = OutstandingCustomersSms.outstandingCustomersSms(row, bearerToken);
				OutstandingCustomersSms.outstandingCustomersSms_validate(res35);*/
				
				
				
//====================================================	


				
				/*401
				Response res11=GenerateAccessToken.generateAccessToken(row, bearerToken);
				GenerateAccessToken.generateAccessToken_validate(res11);
				Thread.sleep(4000);*/
				
				
			/*  manually 500
			 	
			 
				Response res25=DeleteTag.deleteTag(row, bearerToken);
				DeleteTag.deleteTag_validate(res25);
				
				Response res26=AddTag.addTag(row, bearerToken);
				AddTag.addTag_validate(res26);
				
				Response res27=AddFolder.addFolder(row, bearerToken);
				AddFolder.addFolder_validate(res27);
				
				Response res28=DeleteFolder.deleteFolder(row, bearerToken);
				DeleteFolder.deleteFolder_validate(res28);*/
				

				
				
					
					
				
			
				  
				
				
			/*	json to xls
				https://data.page/json/csv
				https://cloudconvert.com/xlsx-to-xls
				==========================================
				json path finder 
				https://jsonpathfinder.com/

				===============================
				JSON code convert online java string with ""
				https://tools.knowledgewalls.com/jsontostring		
			*/
				
				
			}
		}
		

		
		        
	}
	
			
				
				
			
				
				
	
				

				


	
	//@Test(priority=01)
	public void addCustomerInfo() throws Exception{
		System.out.println("adddd");
		test = reports.startTest("addCustomerInfo",	"API TESTING ");
		String uri=baseuri+"addCustomerInfo";
		FileName = "scenario1.xls";		
		String sheetName = "addCustomerInfo";
		int lastrow = TestData.getLastRow("addCustomerInfo");
		
		
		
		TestData = new ExcelNew(System.getProperty("user.dir")+ prop.getProperty("Testdata_path") + FileName);
		String TestData1= System.getProperty("user.dir")+ prop.getProperty("Testdata_path") + FileName;
		String folder= System.getProperty("user.dir")+ prop.getProperty("Testdata_path");
		
		Thread.sleep(1000);	
		
		
		for (int row = 2; row <= lastrow; row++) {

			String RunFlag = TestData.getCellData(sheetName, "Run", row);
			
			if (RunFlag.equals("Y")) {
				
				String bearerToken = TestData.getCellData(sheetName, "Token", row);
				String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
				String lenderAppNumber = TestData.getCellData(sheetName, "lenderAppNumber", row);
				String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
				String lenderRelationshipNumber = TestData.getCellData(sheetName, "lenderRelationshipNumber", row);
				String vdf1 = TestData.getCellData(sheetName, "vdf1", row);


							

				String b = "{\n	\"partnerAccountDetails\": {\n		\"vCardCustomerCode\": \""+vCardCustomerCode+"\",\n		\"lenderAppNumber\": \""+lenderAppNumber+"\",\n		\"lenderAccountNumber\": \""+lenderAccountNumber+"\",\n		\"lenderRelationshipNumber\": \""+lenderRelationshipNumber+"\",\n		\"vdf1\": \""+vdf1+"\",\n		\"vdf2\": \"DEF\",\n		\"vdf3\": \"GHI\",\n		\"vdf4\": \"JKL\",\n		\"vdf5\": \"MNO\"\n	},\n	\"customerInfo\": {\n		\"customerCommonInfo\": {\n			\"firstName\": \"Rajesh\",\n			\"middleName\": \"kumar\",\n			\"lastName\": \"Manthena\",\n			\"gender\": \"MALE\",\n			\"dateOfBirth\": \"2GNWnl3ykTeXVkaFkULvgp6g0Yf3Wv6ls+NN/P6z5RobjIJAp7yHM57QT+z9zcQvfjichZH1P7AbUypGs6BmI+DC3wPawsTbyssjkXPtdsxcgMEUxRFiD8A2waF54F2ZLPh37deLvAwCb3mAMuRJd5qMu1MjcgwGWOQX1o3ZAx5jS9uYhc2x+fE+ukL6PgGEKcsZ7p0kgF07BNNrxxBYUZp9lhZCJC7Q4BsoItvXpaDo3aPVlI652c4idjs3wy7hC4NhqO5Fm9JNfxixaOc0+WoYs7tG7gO/7U0gYvkNnZ6EkY4+9XufLUpU8nDx7Wgjr2jBarJDRYrAmGII83MtAg==\",\n			\"nationality\": \"Indian\",\n			\"educationQualification\": \"3\",\n			\"maritalStatus\": \"1\",\n			\"phoneNbr\": \"ALiogrgTuCeBkgktFOCRtoqcpDmGMCMWTYWtfsxV1w/9QIoWm7ty/Lf9KlRjVlc+pxS2k/Xobnf1Sk/0fzZRIIo8e92DOdoVZUm/XmVgMTAPhvtUWxm6aw+/vff+QHdbh0BJtt2VD0KtGEWFzcBlzOUXPahiIMUvv6p9Hqql6hRjihOzm78GbjbqMRSrf9Xzqh+3/LF7f0t0gqSls91zdDHX93GaGAYeKDlo2tybU9XkEPP/rAAe8XYgaqSIgUhrdI33RY2la3LpC5DADyOKIF4t6iSsnWfBlJjXX2dKT+LtStfl16z0iS2CPsWH+7ncEs6OuCjksto2cK3XhBQpCQ==\",\n			\"emailId\": \"ti2+KunxxC13t8X8HQ0oGMVfA8AG0D9gltb0LdFRNKZjb/PhrRVcQvZSnEZwplS48UmDfwMph+0obD66a6Eyp4VTMjH/uPNS09Osxqw9k8Rhq46izskSMVKGWjFx7QxQkmnOyrt9wdgvq5tMD2viHCIhVWZ3LeUqhM+Ceu2MqVTVshvoM3DwEwJ6ebmueVg3T9NilRhD+cmwF3F285dlJ6SBgJpl+bn5+W7ND5CoPWoIcXYRG2gYknFtw60U0Gug6FE29hPdLVoWqIcIeRBEMVXr1jnfqvZPUomnkOFH5JGa+5mIhA0UZOk2oky+sZlJE2dAwCpVigPhowAYMuSnjw==\",\n			\"fatherName\": \" Ramulu\",\n			\"motherName\": \"Rajeshwari\",\n			\"spouseName\": \"Manasa\",\n			\"idProofType\": \"PAN\",\n			\"idNumber\": \"3qqs/NgQAAh7HBGUy1rMAQXR4x73pMUYdbrmLsYDRxHgRQlTpLwjJq//P+n/bCNToR2BMWGzJGqjAKJUTYKN9rOV9hSgn0DGDnCEI2ep4H2YWuaRq3s5lKAkkdWwJzhDsEAbmL1OyEbAWLyzZB8bJKvs1Q6rb8dkp2gQhEKHOD50xXn3ctutuTxftDoXmtOs0CjhMLhSmkPnqelBP6NhxHS8V7eJNncxTNM5q0dYDVjBfGoeoTN+fvXeoerTZ2pmWcluWo/UQV2gn51Nr2LgB1oo+T9bA+ybdwVxQO+I4/FM7bj8yAcOxohKcwcl/Jk6xDZcePQbJj4rIQUNAxehZg==\"\n		},\n		\"residenceAddressInfo\": {\n			\"addressLine1\": \"Plot no-107\",\n			\"addressLine2\": \"Jawahar colony\",\n			\"city\": \"Hyderabad\",\n			\"state\": \"TS\",\n			\"zipcode\": \"500033\",\n			\"residenceType\": \"NRI\",\n			\"addressProofType\": \"PAN\",\n			\"monthsAtResidence\": 10\n		},\n		\"permanentAddressInfo\": {\n			\"addressLine1\": \"Plot no-107\",\n			\"addressLine2\": \"Jawahar colony\",\n			\"city\": \"Hyderabad\",\n			\"state\": \"TS\",\n			\"zipcode\": \"500033\"\n		},\n		\"employerInfo\": {\n			\"employerName\": \"HMWSSB\",\n			\"employerType\": \"GOV\",\n			\"designation\": \"Emp\",\n			\"officeEmailId\": \"rajesh.manthena@qfund.net\",\n			\"monthsAtCurrentJob\": \"10\",\n			\"totalJobExperience\": \"3\"\n		},\n		\"employerAddressInfo\": {\n			\"addressLine1\": \"Survey no-83\",\n			\"addressLine2\": \"Hakimpet\",\n			\"city\": \"Hyderabad\",\n			\"state\": \"TS\",\n			\"zipcode\": \"500083\"\n		},\n		\"incomeInfo\": {\n			\"incomeType\": \"EM\",\n			\"netMonthlyIncome\": \"gTgZAj+yTlfjmgxrNnDoXiwMDvs88L6/+a27oIg2YbJEwiIUA13nWCbGBFcfHnNWQbYfkGOs73dBp5wRyxF/yXPUDnCUE1s6XEGj7eNM0LUudwncZFxnSle35pqxo49FnMrycJODOwNPpsbZf58BtfWyegmPL9V863r76KBRgb92iVba5l/Jas0HIGgYZJO4e2iCTh5manXw1ip1UnkxTNkBi/dCyQTWmDCO4uHx+iTR/WUA87TE69bhyY+c+5Qbo2eBi51U05a0I1TAcRU9QKPnJuKWuZ3U06uEmLcl2IZF5dc4yMfyinmyzWqaSDY2moEd2v3Fxzaj58Ejrre5GA==\",\n			\"nextPayDate\": \"30/06/2021\"\n		}\n	}\n}";
				
				
				
				Response res=
						given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
						  .when().body(b).post(uri).then().extract().response();
	
				
				
				String ij=String.valueOf(res.getStatusCode());
				if(ij.equals("401")){
					
				}
				
	test.log(LogStatus.INFO, "******************************************************** ");
				
			Headers h1				=res.headers();
			ResponseBody b1			=res.body();				
			Map<String, String> c1	=res.cookies();
			int s1					=res.statusCode();
	test.log(LogStatus.INFO, h1+"********************************************************"+b1
					+"********************************************************"+c1
					+"********************************************************"+s1);		
				
				
			String h2=	res.headers().toString();
			String b2=	res.getBody().asString();///
			String c2=	res.cookies().toString();
			String s2=	String.valueOf(res.getStatusCode());
		

			
			
			
	test.log(LogStatus.INFO, h2+"********************************************************"+b2
					+"********************************************************"+c2
					+"********************************************************"+s2);		
				
			String validate1h =	res.header("Authorization");
			String validate2h =  res.header("date");
		
			JsonPath newData = res.jsonPath();
			 
		     String validate1b = newData.get("status");
		     String validate2b = newData.get("messages.description[0]");
	
		 test.log(LogStatus.INFO, validate1h+"********************************************************"+validate2h
					+"********************************************************"+validate1b
					+"********************************************************"+validate2b);
			
			}
		}
		

		
		        
	}
	
	
	
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws IOException {

		BufferedReader reader;

	reader = new BufferedReader(new FileReader(System.getProperty("user.dir")+"/src/test/java/tests/Objects.properties"));
		prop = new Properties();
		prop.load(reader);
		reader.close();
	
		String Afilename = prop.getProperty("VARD_API_extent_report_file_name") + timestamp + ".html";

		reports = new ExtentReports(System.getProperty("user.dir") + prop.getProperty("VARD_API_extent_report_path") + Afilename,true);

		

	}




	@AfterMethod(alwaysRun = true)
	public void getResult(ITestResult result) throws Exception {


		
		
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, "Test Case Failed is " + result.getName());
		
		}
		else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test Case Skipped is " + result.getName());
		} 
		else if (result.getStatus() == ITestResult.SUCCESS) {

			test.log(LogStatus.PASS, result.getName() + " Test Case is Passed");
		}


		reports.flush();
	

	}



}