package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ApproveDraw extends vcard{
	public static Response approveDraw( int row, String bearerToken ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
		//	String bearerToken = TestData.getCellData(sheetName, "Token", row);
		//	String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
		//	String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
			String drawAmount = TestData.getCellData(sheetName, "drawAmount", row);
			//Double drawAmount=Double.valueOf(drawAmount2);
		//	String vcardTranId = TestData.getCellData(sheetName, "vcardTranId", row);
			
			String vcardTranId = "100";
						
			String payload="{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\",\"drawAmount\":"+drawAmount+",\"vcardTranId\":\""+vcardTranId+"\"}";			
						
		//	System.out.println(payload);
			Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	public static void approveDraw_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

	//	test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
		
		
		if(i.equals("200")){
			 JsonPath newData = res4.jsonPath();
			 	String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
				 authorizationCode = newData.get("authorizationCode"); 
				 test.log(LogStatus.INFO, "RESPONSE of authorizationCode: "+authorizationCode);
				 String status = newData.get("status");
				 test.log(LogStatus.INFO, "RESPONSE of status: "+status);
				 String accountStatus = newData.get("accountStatus");
				 test.log(LogStatus.INFO, "RESPONSE of accountStatus: "+accountStatus);
				 
				 String isApproved = newData.get("isApproved");
				 test.log(LogStatus.INFO, "RESPONSE of isApproved: "+isApproved);
				 
				 
			 
			  }else{
				 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>approveDraw_validate FAIL DUE TO</b>"+message);
					
			 }
		
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>approveDraw_validate PASS</b>");
		
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>approveDraw_validate FAIL DUE TO </b>"+i);
				
			Assert.assertTrue(false);
			
		}
		test.log(LogStatus.INFO, "**************************************************************************************************************** ");
		
	}


}
