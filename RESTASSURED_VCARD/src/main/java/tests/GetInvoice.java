package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class GetInvoice extends vcard{

	public static Response getInvoice( int row ,String bearerToken) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			//String bearerToken = TestData.getCellData(sheetName, "Token", row);
			String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
			String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
			
			String loanTranCode = TestData.getCellData(sheetName, "loanTranCode", row);
			
			
			String payload="{\"seqNbr\":\"\",\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\",\"loanTranCode\":\""+loanTranCode+"\"}";					
			
			
			/*String b="{\"seqNbr\":\"\",\"vCardCustomerCode\":\"zU4WL4MwSMpBoaF2A==\",\"lenderAccountNumber\":\"0ZyRxWKgd9RD==\",\"loanTranCode\":15887988}";					
			*/			
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	

	public static void getInvoice_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>getInvoice_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>getInvoice_validate FAIL</b>");
				
			Assert.assertTrue(false);
			
		}
	}
}
