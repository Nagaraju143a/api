package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class LoanAccountInquiry extends vcard{
	public static Response LoanAccountInquiry( int row,String bearerToken  ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			//String bearerToken = TestData.getCellData(sheetName, "Token", row);
			//String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
			//String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
		
			
			
			
			String payload="{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\"}";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	

	public static void LoanAccountInquiry_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

	//	test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			
			 JsonPath newData = res4.jsonPath();
			 	String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
				 
				 String status = newData.get("status");
				 test.log(LogStatus.INFO, "RESPONSE of status: "+status);
				 
				 String accountStatus = newData.get("accountStatus");
				 test.log(LogStatus.INFO, "RESPONSE of accountStatus: "+accountStatus);

				 test.log(LogStatus.INFO, "RESPONSE of totalLineAmount: "+newData.get("totalLineAmount"));
				 
				 
				// Double availableCreditAmount = newData.get("availableCreditAmount");
				 test.log(LogStatus.INFO, "RESPONSE of availableCreditAmount: "+newData.get("availableCreditAmount"));
				 
				// Double lastPaymentAmount = newData.get("lastPaymentAmount");
				 test.log(LogStatus.INFO, "RESPONSE of lastPaymentAmount: "+newData.get("lastPaymentAmount"));
				 
				 
				// String lastPaymentDate = newData.get("lastPaymentDate");
				 test.log(LogStatus.INFO, "RESPONSE of lastPaymentDate: "+newData.get("lastPaymentDate"));
				// Double minimumAmountDue = newData.get("minimumAmountDue");
				 test.log(LogStatus.INFO, "RESPONSE of minimumAmountDue: "+newData.get("minimumAmountDue"));
				 
				  test.log(LogStatus.INFO, "RESPONSE of interestRate: "+newData.get("interestRate"));
				  
				 
			 }else{
				 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>LoanAccountInquiry_validate FAIL DUE TO</b>"+message);
					
			 }
			
			
			
			
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>LoanAccountInquiry_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>LoanAccountInquiry_validate FAIL DUE TO</b>"+i);
				
			Assert.assertTrue(false);
			
		}

		test.log(LogStatus.INFO, "**************************************************************************************************************** ");
	}

}
