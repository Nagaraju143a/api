package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetEncryptedValue extends vcard {

	public static Response getEncryptedValue( int row, String bearerToken ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);

		//test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+new Exception() .getStackTrace()[0].getMethodName());	
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
				
			
					//	String bearerToken = TestData.getCellData(sheetName, "Token", row);
						String requestTrackCode = TestData.getCellData(sheetName, "requestTrackCode", row);
						String vCardCustomerCodetagName = TestData.getCellData(sheetName, "vCardCustomerCodetagName", row);
						String vCardCustomerCodetagValue = TestData.getCellData(sheetName, "vCardCustomerCodetagValue", row);
						String vCardCustomerCodeVencryptedValue = TestData.getCellData(sheetName, "vCardCustomerCodeVencryptedValue", row);
						String vCardCustomerCodedecryptedValue = TestData.getCellData(sheetName, "vCardCustomerCodedecryptedValue", row);
						String lenderAppNumbertagName = TestData.getCellData(sheetName, "lenderAppNumbertagName", row);
					
						String lenderAppNumbertagValue = vCardCustomerCodetagValue;
						//String lenderAppNumbertagValue = TestData.getCellData(sheetName, "lenderAppNumbertagValue", row);
						String lenderAppNumberencryptedValue = TestData.getCellData(sheetName, "lenderAppNumberencryptedValue", row);
						String lenderAppNumberdecryptedValue = TestData.getCellData(sheetName, "lenderAppNumberdecryptedValue", row);
						String lenderAccountNumbertagName = TestData.getCellData(sheetName, "lenderAccountNumbertagName", row);
						String lenderAccountNumbertagValue = vCardCustomerCodetagValue;
						//String lenderAccountNumbertagValue = TestData.getCellData(sheetName, "lenderAccountNumbertagValue", row);
						String lenderAccountNumberencryptedValue = TestData.getCellData(sheetName, "lenderAccountNumberencryptedValue", row);
						String lenderAccountNumberdecryptedValue = TestData.getCellData(sheetName, "lenderAccountNumberdecryptedValue", row);
						String lenderRelationshipNumbertagName = TestData.getCellData(sheetName, "lenderRelationshipNumbertagName", row);
					
						String lenderRelationshipNumbertagValue =  vCardCustomerCodetagValue;
						//String lenderRelationshipNumbertagValue = TestData.getCellData(sheetName, "lenderRelationshipNumbertagValue", row);
						String lenderRelationshipNumberencryptedValue = TestData.getCellData(sheetName, "lenderRelationshipNumberencryptedValue", row);
						String lenderRelationshipNumberdecryptedValue = TestData.getCellData(sheetName, "lenderRelationshipNumberdecryptedValue", row);
						String dateOfBirthtagName = TestData.getCellData(sheetName, "dateOfBirthtagName", row);
						String dateOfBirthtagValue = TestData.getCellData(sheetName, "dateOfBirthtagValue", row);
						String dateOfBirthencryptedValue = TestData.getCellData(sheetName, "dateOfBirthencryptedValue", row);
						String dateOfBirthdecryptedValue = TestData.getCellData(sheetName, "dateOfBirthdecryptedValue", row);
						String phoneNbrtagName = TestData.getCellData(sheetName, "phoneNbrtagName", row);
						String phoneNbrtagValue = TestData.getCellData(sheetName, "phoneNbrtagValue", row);
						String phoneNbrencryptedValue = TestData.getCellData(sheetName, "phoneNbrencryptedValue", row);
						String phoneNbrdecryptedValue = TestData.getCellData(sheetName, "phoneNbrdecryptedValue", row);
						String idNumbertagName = TestData.getCellData(sheetName, "idNumbertagName", row);
						String idNumbertagValue = TestData.getCellData(sheetName, "idNumbertagValue", row);
						String idNumberencryptedValue = TestData.getCellData(sheetName, "idNumberencryptedValue", row);
						String idNumberdecryptedValue = TestData.getCellData(sheetName, "idNumberdecryptedValue", row);
						String ifscCodetagName = TestData.getCellData(sheetName, "ifscCodetagName", row);
						String ifscCodetagValue = TestData.getCellData(sheetName, "ifscCodetagValue", row);
						String ifscCodeencryptedValue = TestData.getCellData(sheetName, "ifscCodeencryptedValue", row);
						String ifscCodedecryptedValue = TestData.getCellData(sheetName, "ifscCodedecryptedValue", row);
						String accountNbrtagName = TestData.getCellData(sheetName, "accountNbrtagName", row);
						String accountNbrtagValue = TestData.getCellData(sheetName, "accountNbrtagValue", row);
						String accountNbrencryptedValue = TestData.getCellData(sheetName, "accountNbrencryptedValue", row);
						String accountNbrdecryptedValue = TestData.getCellData(sheetName, "accountNbrdecryptedValue", row);
						String emailIdtagName = TestData.getCellData(sheetName, "emailIdtagName", row);
						String emailIdtagValue = TestData.getCellData(sheetName, "emailIdtagValue", row);
						String emailIdencryptedValue = TestData.getCellData(sheetName, "emailIdencryptedValue", row);
						String emailIddecryptedValue = TestData.getCellData(sheetName, "emailIddecryptedValue", row);
						String netMonthlyIncometagName = TestData.getCellData(sheetName, "netMonthlyIncometagName", row);
						String netMonthlyIncometagValue = TestData.getCellData(sheetName, "netMonthlyIncometagValue", row);
						String netMonthlyIncomeencryptedValue = TestData.getCellData(sheetName, "netMonthlyIncomeencryptedValue", row);
						String netMonthlyIncomedecryptedValue = TestData.getCellData(sheetName, "netMonthlyIncomedecryptedValue", row);
						String availableLimittagName = TestData.getCellData(sheetName, "availableLimittagName", row);
						String availableLimittagValue = TestData.getCellData(sheetName, "availableLimittagValue", row);
						String availableLimitencryptedValue = TestData.getCellData(sheetName, "availableLimitencryptedValue", row);
						String availableLimitdecryptedValue = TestData.getCellData(sheetName, "availableLimitdecryptedValue", row);
							
		String b="{\"requestTrackCode\":\""+requestTrackCode+"\",\"tags\":[{\"tagName\":\""+vCardCustomerCodetagName+"\",\"tagValue\":\""+vCardCustomerCodetagValue+"\",\"encryptedValue\":\""+vCardCustomerCodeVencryptedValue+"\",\"decryptedValue\":\""+vCardCustomerCodedecryptedValue+"\"},{\"tagName\":\""+lenderAppNumbertagName+"\",\"tagValue\":\""+lenderAppNumbertagValue+"\",\"encryptedValue\":\""+lenderAppNumberencryptedValue+"\",\"decryptedValue\":\""+lenderAppNumberdecryptedValue+"\"},{\"tagName\":\""+lenderAccountNumbertagName+"\",\"tagValue\":\""+lenderAccountNumbertagValue+"\",\"encryptedValue\":\""+lenderAccountNumberencryptedValue+"\",\"decryptedValue\":\""+lenderAccountNumberdecryptedValue+"\"},{\"tagName\":\""+lenderRelationshipNumbertagName+"\",\"tagValue\":\""+lenderRelationshipNumbertagValue+"\",\"encryptedValue\":\""+lenderRelationshipNumberencryptedValue+"\",\"decryptedValue\":\""+lenderRelationshipNumberdecryptedValue+"\"},{\"tagName\":\""+dateOfBirthtagName+"\",\"tagValue\":\""+dateOfBirthtagValue+"\",\"encryptedValue\":\""+dateOfBirthencryptedValue+"\",\"decryptedValue\":\""+dateOfBirthdecryptedValue+"\"},{\"tagName\":\""+phoneNbrtagName+"\",\"tagValue\":\""+phoneNbrtagValue+"\",\"encryptedValue\":\""+phoneNbrencryptedValue+"\",\"decryptedValue\":\""+phoneNbrdecryptedValue+"\"},{\"tagName\":\""+idNumbertagName+"\",\"tagValue\":\""+idNumbertagValue+"\",\"encryptedValue\":\""+idNumberencryptedValue+"\",\"decryptedValue\":\""+idNumberdecryptedValue+"\"},{\"tagName\":\""+ifscCodetagName+"\",\"tagValue\":\""+ifscCodetagValue+"\",\"encryptedValue\":\""+ifscCodeencryptedValue+"\",\"decryptedValue\":\""+ifscCodedecryptedValue+"\"},{\"tagName\":\""+accountNbrtagName+"\",\"tagValue\":\""+accountNbrtagValue+"\",\"encryptedValue\":\""+accountNbrencryptedValue+"\",\"decryptedValue\":\""+accountNbrdecryptedValue+"\"},{\"tagName\":\""+emailIdtagName+"\",\"tagValue\":\""+emailIdtagValue+"\",\"encryptedValue\":\""+emailIdencryptedValue+"\",\"decryptedValue\":\""+emailIddecryptedValue+"\"},{\"tagName\":\""+netMonthlyIncometagName+"\",\"tagValue\":\""+netMonthlyIncometagValue+"\",\"encryptedValue\":\""+netMonthlyIncomeencryptedValue+"\",\"decryptedValue\":\""+netMonthlyIncomedecryptedValue+"\"},{\"tagName\":\""+availableLimittagName+"\",\"tagValue\":\""+availableLimittagValue+"\",\"encryptedValue\":\""+availableLimitencryptedValue+"\",\"decryptedValue\":\""+availableLimitdecryptedValue+"\"}]}";

					
						
						
						Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
											
						
			
				return res;
	
	}

	public static void getEncryptedValue_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

	//	test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
				JsonPath newData = res4.jsonPath();
			 	vCardCustomerCode = newData.get("tags[0].encryptedValue");
			 	test.log(LogStatus.INFO, "vCardCustomerCode encryptedValue : "+vCardCustomerCode);
			 	 String vcc = newData.get("tags[0].decryptedValue");
			 	test.log(LogStatus.INFO, "vCardCustomerCode  decryptedValue : "+vcc);
			 	lenderAppNumber = newData.get("tags[1].encryptedValue");
			 	test.log(LogStatus.INFO, "lenderAppNumber encryptedValue : "+lenderAppNumber);
			 	lenderAccountNumber = newData.get("tags[2].encryptedValue");
			 	test.log(LogStatus.INFO, "lenderAccountNumber encryptedValue : "+lenderAccountNumber);
			 	
			 	lenderRelationshipNumber = newData.get("tags[3].encryptedValue");
			 	test.log(LogStatus.INFO, "lenderRelationshipNumber encryptedValue : "+lenderRelationshipNumber);
			 	
			 	dateOfBirth = newData.get("tags[4].encryptedValue");
			 	test.log(LogStatus.INFO, "dateOfBirth encryptedValue : "+dateOfBirth);
			 	phoneNbr= newData.get("tags[5].encryptedValue");
			 	test.log(LogStatus.INFO, "phoneNbr encryptedValue : "+phoneNbr);
			 	idNumber=newData.get("tags[6].encryptedValue");
			 	test.log(LogStatus.INFO, "idNumber encryptedValue : "+idNumber);
			 	ifscCode=newData.get("tags[7].encryptedValue");
			 	test.log(LogStatus.INFO, "ifscCode encryptedValue : "+ifscCode);
			 	emailId= newData.get("tags[9].encryptedValue");
			 	test.log(LogStatus.INFO, "emailId encryptedValue : "+emailId);
			 	netMonthlyIncome=newData.get("tags[10].encryptedValue");
			 	test.log(LogStatus.INFO, "netMonthlyIncome encryptedValue : "+netMonthlyIncome);
			 	
			 	AccountNumberTAG = newData.get("tags[8].tagValue");
			 	test.log(LogStatus.INFO, "AccountNumberTAG encryptedValue : "+AccountNumberTAG);
			 
						
			 	
			 	 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>getEncryptedValue PASS</b>");
					
				  	
		        
		  
			 
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>getEncryptedValue FAIL</b>");
				
			Assert.assertTrue(false);
			
		}
		test.log(LogStatus.INFO, "******************************************************** ");
	}
	
	
}
