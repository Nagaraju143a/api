package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Draw extends vcard{
	
	public static Response draw( int row ,String bearerToken) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			//String bearerToken = TestData.getCellData(sheetName, "Token", row);
		//	String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
		//	String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
			String merchantName = TestData.getCellData(sheetName, "merchantName", row);
			String merchantCity = TestData.getCellData(sheetName, "merchantCity", row);
			String merchantCategory = TestData.getCellData(sheetName, "merchantCategory", row);
		//	String authorizationCode = TestData.getCellData(sheetName, "authorizationCode", row);
			String transReferenceNumber = TestData.getCellData(sheetName, "transReferenceNumber", row);
			String tranDate = TestData.getCellData(sheetName, "tranDate", row);
			String description = TestData.getCellData(sheetName, "description", row);
			String drawAmount = TestData.getCellData(sheetName, "drawAmount", row);
			String longitude = TestData.getCellData(sheetName, "longitude", row);
			String latitude = TestData.getCellData(sheetName, "latitude", row);
			String transCategory = TestData.getCellData(sheetName, "transCategory", row);
			String tranStatus = TestData.getCellData(sheetName, "tranStatus", row);
			String vcardTranId = TestData.getCellData(sheetName, "vcardTranId", row);
			String tranType = TestData.getCellData(sheetName, "tranType", row);
			String transMode = TestData.getCellData(sheetName, "transMode", row);
			String failureReason = TestData.getCellData(sheetName, "failureReason", row);
			String vpa = TestData.getCellData(sheetName, "vpa", row);
			String benName = TestData.getCellData(sheetName, "benName", row);
			String bankName = TestData.getCellData(sheetName, "bankName", row);
			String accNum = TestData.getCellData(sheetName, "accNum", row);
			String ifsc = TestData.getCellData(sheetName, "ifsc", row);
			String bankRefNum = TestData.getCellData(sheetName, "bankRefNum", row);
			String upiRefNum = TestData.getCellData(sheetName, "upiRefNum", row);
			String tagList = TestData.getCellData(sheetName, "tagList", row);
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												
		//String 	authorizationCode=authorizationCode;//from approvaldraw
			
			String payload="{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\",\"merchantName\":\""+merchantName+"\",\"merchantCity\":\""+merchantCity+"\",\"merchantCategory\":\""+merchantCategory+"\",\"authorizationCode\":\""+authorizationCode+"\",\"transReferenceNumber\":\""+transReferenceNumber+"\",\"tranDate\":\""+tranDate+"\",\"description\":\""+description+"\",\"drawAmount\":"+drawAmount+",\"longitude\":"+longitude+",\"latitude\":"+latitude+",\"transCategory\":\""+transCategory+"\",\"tranStatus\":\""+tranStatus+"\",\"vcardTranId\":\""+vcardTranId+"\",\"tranType\":\""+tranType+"\",\"transMode\":\""+transMode+"\",\"failureReason\":\""+failureReason+"\",\"vpa\":\""+vpa+"\",\"benName\":\""+benName+"\",\"bankName\":\""+bankName+"\",\"accNum\":\""+accNum+"\",\"ifsc\":\""+ifsc+"\",\"bankRefNum\":\""+bankRefNum+"\",\"upiRefNum\":\""+upiRefNum+"\",\"tagList\":\""+tagList+"\"}";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	public static void draw_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

	//	test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 JsonPath newData = res4.jsonPath();
			 	String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
				  String status = newData.get("status");
				 test.log(LogStatus.INFO, "RESPONSE of status: "+status);
				 String accountStatus = newData.get("accountStatus");
				 test.log(LogStatus.INFO, "RESPONSE of accountStatus: "+accountStatus);
				 
				 String loanTranCode = newData.get("loanTranCode");
				 test.log(LogStatus.INFO, "RESPONSE of loanTranCode: "+loanTranCode);
				 
				 
			 
			  }else{
				  test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>draw_validate FAIL DUE TO</b>"+message);
					
			 }
		
			
			
			 
			
			
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>draw_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>draw_validate FAIL DUE TO</b>"+i);
				
			Assert.assertTrue(false);
			
		}
		test.log(LogStatus.INFO, "**************************************************************************************************************** ");
		
	}

}
