package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class EmandateRegistrationBounce extends vcard{
	
	public static Response emandateRegistrationBounce( int row, String bearerToken ) throws Exception{

	String method= new Exception() .getStackTrace()[0].getMethodName();
	test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
	
	
		String sheetName=method;
		int lastrow=TestData.getLastRow(method);
				
		//String bearerToken = TestData.getCellData(sheetName, "Token", row);
		String vc = TestData.getCellData(sheetName, "vCardCustomerCode", row);
		String ln = TestData.getCellData(sheetName, "loanNumber", row);
		String an = TestData.getCellData(sheetName, "accountNumber", row);
		
		String rd = TestData.getCellData(sheetName, "requestDate", row);
		
		String ammount = TestData.getCellData(sheetName, "requestAmount", row);
		
		int i =Integer.parseInt(ammount);
	
		
		
String payload="{\"vCardCustomerCode\":\""+vc+"\",\"loanNumber\":\""+ln+"\",\"accountNumber\":\""+an+"\",\"requestDate\":\""+rd+"\",\"requestAmount\":"+i+"}";					
			
						Response res=
							given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
							  .when().body(payload).post(baseuri+"eMandateCustomerRequest").then().extract().response();
					
					
					
		
			return res;

}
	public static void emandateRegistrationBounce_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>emandateRegistrationBounce_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>emandateRegistrationBounce_validate FAIL</b>");
				
			Assert.assertTrue(false);
			
		}
	}
}
