package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class OriginateLoan  extends vcard{

	public static Response originateLoan( int row ,String bearerToken ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						
						//String bearerToken = TestData.getCellData(sheetName, "Token", row);
					//	String vcd = TestData.getCellData(sheetName, "vCardCustomerCode", row);
					//	String la = TestData.getCellData(sheetName, "lenderAccountNumber", row);
					
						
						String C = TestData.getCellData(sheetName, "loanCategory", row);
						String ls = TestData.getCellData(sheetName, "loanStartDate", row);
						String lv = TestData.getCellData(sheetName, "loanValidityDate", row);
						String  ammount1 = TestData.getCellData(sheetName, "approvedLimit", row);
						Double ammount=Double.valueOf(ammount1);
						String al = TestData.getCellData(sheetName, "availableLimit", row);
						String v1 = TestData.getCellData(sheetName, "vdf1", row);
						String v2 = TestData.getCellData(sheetName, "vdf2", row);
						String v3 = TestData.getCellData(sheetName, "vdf3", row);
						String v4 = TestData.getCellData(sheetName, "vdf4", row);
						String v5 = TestData.getCellData(sheetName, "vdf5", row);
																	
						String vcd = vCardCustomerCode;
						String la =lenderAccountNumber;
						String payload="{\"vCardCustomerCode\":\""+vcd+"\",\"lenderAccountNumber\":\""+la+"\",\"loanCategory\":\""+C+"\",\"loanStartDate\":\""+ls+"\",\"loanValidityDate\":\""+lv+"\", \"approvedLimit\": "+ammount+",\"availableLimit\":\""+al+"\",\"vdf1\":\""+v1+"\",\"vdf2\":\""+v2+"\",\"vdf3\":\""+v3+"\",\"vdf4\":\""+v4+"\",\"vdf5\":\""+v5+"\"}";
			
							
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}
	

	public static void originateLoan_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		//test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			JsonPath newData = res4.jsonPath();
			String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
				 String status = newData.get("status");
				 test.log(LogStatus.INFO, "RESPONSE of status: "+status);
				 loannumber = newData.get("loanNumber");
				 test.log(LogStatus.INFO, "RESPONSE of loannumber: "+loannumber);	
			 }else{
				 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>originateLoan_validate FAIL DUE TO </b>"+message);
						 
			 }
			
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>originateLoan_validate PASS</b>");
			 
			 
			 
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>originateLoan_validate FAIL DUE TO </b>"+i);
				
			Assert.assertTrue(false);
			
		}
		test.log(LogStatus.INFO, "******************************************************** ");
	}
	
	
}