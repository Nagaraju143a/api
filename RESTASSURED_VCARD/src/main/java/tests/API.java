package tests;
//This class contains methods for login and logout functionality

import static io.restassured.RestAssured.given;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class API extends vcard{
	
	public static Response closeAccount( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	public static Response suspendAccount( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	public static Response getTransactionDetails( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	public static Response getLoanDashBoardDetails( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	public static Response deleteFolder( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	public static Response addFolder( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	public static Response addTag( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	public static Response deleteTag( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	public static Response loanPayments( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	
	public static Response loanTransactions( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	public static Response payOffAmount( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	public static Response addPtpAmountDetails( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	
	public static Response emailStatement( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	public static Response sendStatement( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	
	public static Response loanTransactionsByStatement( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	
	public static Response listStatements( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}


	
	
	public static Response getEmiPlans( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	

	
	
	
	
	
	
	
	
	
	public static Response emiConversion( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	
	
	public static Response makePaymentForvCard( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}
	
	
	public static Response getInvoice( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	public static Response addInvoice( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}


	
	public static Response LoanAccountInquiry( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	
	public static Response generateAccessToken( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);

						
			String b="";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}

	
	public static Response draw( int row ) throws Exception{

	String method= new Exception() .getStackTrace()[0].getMethodName();
	test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
	
	
		String sheetName=method;
		int lastrow=TestData.getLastRow(method);
				
		String bearerToken = TestData.getCellData(sheetName, "Token", row);

					
		String b="";			
					
						Response res=
							given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
							  .when().body(b).post(baseuri+method).then().extract().response();
					
					
					
		
			return res;

}

	
	public static Response approveDraw( int row ) throws Exception{

	String method= new Exception() .getStackTrace()[0].getMethodName();
	test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
	
	
		String sheetName=method;
		int lastrow=TestData.getLastRow(method);
				
		String bearerToken = TestData.getCellData(sheetName, "Token", row);

					
		String b="";			
					
						Response res=
							given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
							  .when().body(b).post(baseuri+method).then().extract().response();
					
					
					
		
			return res;

}
	
	public static Response viewCustomerProfile( int row ) throws Exception{

	String method= new Exception() .getStackTrace()[0].getMethodName();
	test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
	
	
		String sheetName=method;
		int lastrow=TestData.getLastRow(method);
				
		String bearerToken = TestData.getCellData(sheetName, "Token", row);

					
		String b="";			
					
						Response res=
							given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
							  .when().body(b).post(baseuri+method).then().extract().response();
					
					
					
		
			return res;

}

	
	

		public static Response activateAccount( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			String bearerToken = TestData.getCellData(sheetName, "Token", row);
			String vcd = TestData.getCellData(sheetName, "vCardCustomerCode", row);			
			String la = TestData.getCellData(sheetName, "lenderAccountNumber", row);
						/*String b = "{\n   \"vCardCustomerCode\": \"Li5KMKU+gPJGI6+sv8CgVjC+RrQoAwpn7XwKXEB4t7ofjgMVobktQ8/VO3xRgi9XJHvsfp6mQnVp2VjVARjgBsuVym0mvGP0dDjf+2CPV9puY9maVcWxPI9CZULCt0GSUYXdV5HKgpypp1MdXCvDNkRQg3K80ABYgymUTywCsqKaJ5XaRr5DqsUMaB+V0oSujydMjK6xd9lFem689trrqR2u+Hf5Gp+vzdX5GJoK7xKkMoByPh7NPG74ryS6YV/vhUSr3//Q9ytnCRYvlE9siAbO3mGF5zFn3S6MYAXTaV74YkpHUXiY/hnVN+ZzmjOiPFBJ3l45HzsAjGsvL9iPxw==\",\n    \"lenderAccountNumber\": \"nDPu9yLoDNvoCZWQc688ohpmcmmrVzL9Z8OROPvvn1MmFNsLbSHSYyzj5EFDBb2HCo6AYif0WLT15PlSu6wlMxwK6MxeGR58FsqGVQsSsbQLbQQbIEHeHQ0e/qPd8s2E4WG/vEOh+xbF+CMUM75mNkiPjj1/lPu6NdghbRXQj+0/ct60nmHLUyM8phedVbKl/KVfKpCjnoaLPIqMochsTQ49TDYEv7C4kauC1LE9J8w/W5Uodykc+2vCJP99kZ5+KLVBnZ0VgPE+OR3XjLf/DMHe3UA1qYrpHo4J1AZjMteDO1/BF2sZFLftmAeGImJiHL8XElXO2BXhRADcCzLcHA==\"\n}";
						*/
						
			String b="{\"vCardCustomerCode\":\""+vcd+"\",\"lenderAccountNumber\":\""+la+"\"}";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}


	
	
	public static Response originateLoan( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						
						String bearerToken = TestData.getCellData(sheetName, "Token", row);
						String vcd = TestData.getCellData(sheetName, "Token", row);
						String la = TestData.getCellData(sheetName, "Token", row);
						String C = TestData.getCellData(sheetName, "Token", row);
						String ls = TestData.getCellData(sheetName, "Token", row);
						String lv = TestData.getCellData(sheetName, "Token", row);
						String ammount = TestData.getCellData(sheetName, "Token", row);
						
						String al = TestData.getCellData(sheetName, "Token", row);
						String v1 = TestData.getCellData(sheetName, "Token", row);
						String v2 = TestData.getCellData(sheetName, "Token", row);
						String v3 = TestData.getCellData(sheetName, "Token", row);
						String v4 = TestData.getCellData(sheetName, "Token", row);
						String v5 = TestData.getCellData(sheetName, "Token", row);
					
						
					/*	
						String b = "{\n \"vCardCustomerCode\": \"Li5KMKU+gPJGI6+sv8CgVjC+RrQoAwpn7XwKXEB4t7ofjgMVobktQ8/VO3xRgi9XJHvsfp6mQnVp2VjVARjgBsuVym0mvGP0dDjf+2CPV9puY9maVcWxPI9CZULCt0GSUYXdV5HKgpypp1MdXCvDNkRQg3K80ABYgymUTywCsqKaJ5XaRr5DqsUMaB+V0oSujydMjK6xd9lFem689trrqR2u+Hf5Gp+vzdX5GJoK7xKkMoByPh7NPG74ryS6YV/vhUSr3//Q9ytnCRYvlE9siAbO3mGF5zFn3S6MYAXTaV74YkpHUXiY/hnVN+ZzmjOiPFBJ3l45HzsAjGsvL9iPxw==\",\n	\"lenderAccountNumber\": \"nDPu9yLoDNvoCZWQc688ohpmcmmrVzL9Z8OROPvvn1MmFNsLbSHSYyzj5EFDBb2HCo6AYif0WLT15PlSu6wlMxwK6MxeGR58FsqGVQsSsbQLbQQbIEHeHQ0e/qPd8s2E4WG/vEOh+xbF+CMUM75mNkiPjj1/lPu6NdghbRXQj+0/ct60nmHLUyM8phedVbKl/KVfKpCjnoaLPIqMochsTQ49TDYEv7C4kauC1LE9J8w/W5Uodykc+2vCJP99kZ5+KLVBnZ0VgPE+OR3XjLf/DMHe3UA1qYrpHo4J1AZjMteDO1/BF2sZFLftmAeGImJiHL8XElXO2BXhRADcCzLcHA==\",\n  \"loanCategory\": \"C\",\n  \"loanStartDate\": \"01/03/2020\",\n  \"loanValidityDate\": \"30/06/2021\",\n  \"approvedLimit\": 30000.00,\n  \"availableLimit\":\"fD+UmrVftctsxhKY4mtXWWMmivxVFq29EecaPsY5jn5AHjViW8hgDAUVVXuzPuffa0v1Gtq72/rGbwGmr7b3m5jVHuEKI2+F3Ibq5XxIaeSPiwakHcOgIO343GyMvG9tnlTX/nSs1SvQPEwgpaOxQL72Hdt/BIhCC0oIHPcW2Rqg9Qo6hip9aNJQljilI00FImapsF6Do6gbSDsaHcWn8sCfL/0Hl8bxcy0PcDYGge5Wz2rtAZA19yswj9kAbrXPHIVHVxccanWjZwgUcPDirz1aGvfvmomTWl4g8XKFDuvgyRf93+Ld4vlR904r8VlTXyC/hMeNpO60PwRkuRHkUA==\",\n  \"vdf1\": \"Cokarma\",\n	\"vdf2\": \"DarkHorse\",\n	\"vdf3\": \"Ness\",\n	\"vdf4\": \"Hyrezy Talent\",\n	\"vdf5\": \"GrabOn\"\n\n}";
						
						*/
				String b="{\"vCardCustomerCode\":\""+vcd+"\",\"lenderAccountNumber\":\""+la+"\",\"loanCategory\":\""+C+"\",\"loanStartDate\":\""+ls+"\",\"loanValidityDate\":\""+lv+"\",\n  \"approvedLimit\": "+ammount+",\n ,\"availableLimit\":\""+al+"\",\"vdf1\":\""+v1+"\",\"vdf2\":\""+v2+"\",\"vdf3\":\""+v3+"\",\"vdf4\":\""+v4+"\",\"vdf5\":\""+v5+"\"}";
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}

	
	
	
	
	public static Response eMandateCustomerRequest( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						
						String bearerToken = TestData.getCellData(sheetName, "Token", row);
						String vc = TestData.getCellData(sheetName, "vCardCustomerCode", row);
						String ln = TestData.getCellData(sheetName, "loanNumber", row);
						String an = TestData.getCellData(sheetName, "accountNumber", row);
						
						String rd = TestData.getCellData(sheetName, "requestDate", row);
						
						String ammount = TestData.getCellData(sheetName, "requestAmount", row);
						
						int i =Integer.parseInt(ammount);
					/*	String b ="{\n  \"vCardCustomerCode\": \"oBShuF1nPPdk2iZGgymBYkTU/HlI75JeZdnx5pZvSSX0sM/xC/44vWJWlp6wH9Z88+VsDTpFtWlo7247+TMm26odImQpQ8UjOM9NYtUZP1QiuItI9g367r/nbl+Qqoa7Z3ntvyMYLeu+b5WGfN1BTTTY4GbJRTjX6gpAJ+zqHSVwLH6rnlAcTBiLxOwkq58185jntERHNWWejomvhm0CdORUg3gX1+Mm3SS8qYzzmir3pM++3JKG0XrktXyHN8ILwgvDbaTkoua0ACh8yk7NO9nM7coEyFKfAcRdDw/Cazr/QCevzhXQjMc9/JUOUe6gIZxQ/zU4WL4MwSMpBoaF2A==\",\n  \"loanNumber\": \"1000747693\",\n  \"accountNumber\": \"113498779\",\n  \"requestDate\": \"02/02/2021\",\n  \"requestAmount\":100\n}";

						*/
						
						
	String b="{\"vCardCustomerCode\":\""+vc+"\",\"loanNumber\":\""+ln+"\",\"accountNumber\":\""+an+"\",\"requestDate\":\""+rd+"\",\"requestAmount\":"+i+"}";					
				
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}

	
	
	public static Response getDecryptedValue( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						
						String bearerToken = TestData.getCellData(sheetName, "Token", row);
						String vcd = TestData.getCellData(sheetName, "Token", row);
						String tagd = TestData.getCellData(sheetName, "Token", row);
						
						/*String b = "{\n	\"tags\": [\n\n		{\n			\"tagName\": \"vCardCustomerCode\",\n			\"tagValue\": \"bBz8tz43sSqmQkY+gcSI2cSDegglaehIzMVsqGsjKoK1JE9ILZ9yGk50S7ZmvAni+Lz3nEQLe2uvkL5aiiGwAKMIfTlANcaXWMPu6RN3XpW9pAW98j8hnSAm//13KSMbjX3qKr+WREqcTgcKLeInek6q8ttQn9aDQ+D2OaK9avI9k/eO4s8RusNY41dN1QJN9hPrSTz1s8RBNQxWEAIHQz50yWs1ZOzNGR1WIzo5EgWpbMKLsBIc0GFRt3s5RTKG8lrQ1APgfX9qVyhR+5Xes233clWxU1KUcXG3EwjGdrgBktpy+vjLDuMRNxb39skL+fWCj0cSzmqFYbGALBLChA==\"\n		}\n\n\n	]\n}";
						*/
						
	String b="{\"tags\":[{\"tagName\":\""+vcd+"\",\"tagValue\":\""+tagd+"\"}]}";					
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}

	
	
	
	

	
	public static Response addBankInfo( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						
						String bearerToken = TestData.getCellData(sheetName, "Token", row);
						String vcd = TestData.getCellData(sheetName, "vCardCustomerCode", row);
						String ifsc = TestData.getCellData(sheetName, "ifscCode", row);
						String an = TestData.getCellData(sheetName, "accountHolderName", row);
						String anum = TestData.getCellData(sheetName, "accountNbr", row);
						String br = TestData.getCellData(sheetName, "bankReferenceId", row);


			/*			String b = "{\n   \"vCardCustomerCode\": \"Li5KMKU+gPJGI6+sv8CgVjC+RrQoAwpn7XwKXEB4t7ofjgMVobktQ8/VO3xRgi9XJHvsfp6mQnVp2VjVARjgBsuVym0mvGP0dDjf+2CPV9puY9maVcWxPI9CZULCt0GSUYXdV5HKgpypp1MdXCvDNkRQg3K80ABYgymUTywCsqKaJ5XaRr5DqsUMaB+V0oSujydMjK6xd9lFem689trrqR2u+Hf5Gp+vzdX5GJoK7xKkMoByPh7NPG74ryS6YV/vhUSr3//Q9ytnCRYvlE9siAbO3mGF5zFn3S6MYAXTaV74YkpHUXiY/hnVN+ZzmjOiPFBJ3l45HzsAjGsvL9iPxw==\",\n  \"ifscCode\": \"p/9LfxSUdCimgS9IXfqumT4/QDwMHRL9SwtzNyOTSiT1qx9AkRw8Gv/8BVZSeWKvv9LlJ3UJpAZ+SdJOZ3N9wfjcPwuZbR52hHZhSKtyDGlylarTTJf8cWWn/V9E5X2VDrZZ+7DK8c8LZolTaNGMy64+M+iSJetLoUhsOS2JRcfhxEW9IpKdFfYo5SHyUhw1dZahN3ikTSmgOnl0MBasnb4bTsnlnF9Fq/AERxVFTs+jDvCdYEC/bVSAWuKEJpTkRWP7gdAmGSB9eVWsF0USJZr9xZrEnstsT43Tjj7ED4Mo4MEP3LTBNMexMfMEr3Q70Qcv9QZTrcXU5J/mWcMmXQ==\",\n  \"accountHolderName\": \"Rajesh\",\n  \"accountNbr\": \"1ua97Bqbvf8bIDYIO1GiPRDpeIgmytP87oUJ5pHPtbXs2JuiZyxVBfA0FYUgsObcqSXVs40pbCBu63tYuzSVyorMAP0nJgjxGrFI1onlL2wRV4yR/uO+4Gg+eTWi2FKjsBP5iaGPAlaXS2hVLUbzGSqF5KzevOjBV6DzVx2qohhFrlJc6yumWx+u73UltzoXeZ4kHttMvgv9IAmyREIm1I1GZvC4FYVOy32UaKi29MIGLSstbkf2IlcwVFlgynI3BNVfu/Nfi3sGszBABFG3eRN+ZKIam8BoHnRd7cX3MAm/olxBDPYDaiX2vqHK1WMEoM1DoGS8OmPEtf20ZGaQjQ==\",\n  \"bankReferenceId\": \"500033\"\n}\n";
		
	*/
						
						
						String b="{\"vCardCustomerCode\":\""+vcd+"\",\"ifscCode\":\""+ifsc+"\",\"accountHolderName\":\""+an+"\",\"accountNbr\":\""+anum+"\",\"bankReferenceId\":\""+br+"\"}";
						
						Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}

	
	
	
	
	
	
	public static Response getEncryptedValue( int row ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						String bearerToken = TestData.getCellData(sheetName, "Token", row);
						String requestTrackCode = TestData.getCellData(sheetName, "requestTrackCode", row);
						String vCardCustomerCodetagName = TestData.getCellData(sheetName, "vCardCustomerCodetagName", row);
						String vCardCustomerCodetagValue = TestData.getCellData(sheetName, "vCardCustomerCodetagValue", row);
						String vCardCustomerCodeVencryptedValue = TestData.getCellData(sheetName, "vCardCustomerCodeVencryptedValue", row);
						String vCardCustomerCodedecryptedValue = TestData.getCellData(sheetName, "vCardCustomerCodedecryptedValue", row);
						String lenderAppNumbertagName = TestData.getCellData(sheetName, "lenderAppNumbertagName", row);
						String lenderAppNumbertagValue = TestData.getCellData(sheetName, "lenderAppNumbertagValue", row);
						String lenderAppNumberencryptedValue = TestData.getCellData(sheetName, "lenderAppNumberencryptedValue", row);
						String lenderAppNumberdecryptedValue = TestData.getCellData(sheetName, "lenderAppNumberdecryptedValue", row);
						String lenderAccountNumbertagName = TestData.getCellData(sheetName, "lenderAccountNumbertagName", row);
						String lenderAccountNumbertagValue = TestData.getCellData(sheetName, "lenderAccountNumbertagValue", row);
						String lenderAccountNumberencryptedValue = TestData.getCellData(sheetName, "lenderAccountNumberencryptedValue", row);
						String lenderAccountNumberdecryptedValue = TestData.getCellData(sheetName, "lenderAccountNumberdecryptedValue", row);
						String lenderRelationshipNumbertagName = TestData.getCellData(sheetName, "lenderRelationshipNumbertagName", row);
						String lenderRelationshipNumbertagValue = TestData.getCellData(sheetName, "lenderRelationshipNumbertagValue", row);
						String lenderRelationshipNumberencryptedValue = TestData.getCellData(sheetName, "lenderRelationshipNumberencryptedValue", row);
						String lenderRelationshipNumberdecryptedValue = TestData.getCellData(sheetName, "lenderRelationshipNumberdecryptedValue", row);
						String dateOfBirthtagName = TestData.getCellData(sheetName, "dateOfBirthtagName", row);
						String dateOfBirthtagValue = TestData.getCellData(sheetName, "dateOfBirthtagValue", row);
						String dateOfBirthencryptedValue = TestData.getCellData(sheetName, "dateOfBirthencryptedValue", row);
						String dateOfBirthdecryptedValue = TestData.getCellData(sheetName, "dateOfBirthdecryptedValue", row);
						String phoneNbrtagName = TestData.getCellData(sheetName, "phoneNbrtagName", row);
						String phoneNbrtagValue = TestData.getCellData(sheetName, "phoneNbrtagValue", row);
						String phoneNbrencryptedValue = TestData.getCellData(sheetName, "phoneNbrencryptedValue", row);
						String phoneNbrdecryptedValue = TestData.getCellData(sheetName, "phoneNbrdecryptedValue", row);
						String idNumbertagName = TestData.getCellData(sheetName, "idNumbertagName", row);
						String idNumbertagValue = TestData.getCellData(sheetName, "idNumbertagValue", row);
						String idNumberencryptedValue = TestData.getCellData(sheetName, "idNumberencryptedValue", row);
						String idNumberdecryptedValue = TestData.getCellData(sheetName, "idNumberdecryptedValue", row);
						String ifscCodetagName = TestData.getCellData(sheetName, "ifscCodetagName", row);
						String ifscCodetagValue = TestData.getCellData(sheetName, "ifscCodetagValue", row);
						String ifscCodeencryptedValue = TestData.getCellData(sheetName, "ifscCodeencryptedValue", row);
						String ifscCodedecryptedValue = TestData.getCellData(sheetName, "ifscCodedecryptedValue", row);
						String accountNbrtagName = TestData.getCellData(sheetName, "accountNbrtagName", row);
						String accountNbrtagValue = TestData.getCellData(sheetName, "accountNbrtagValue", row);
						String accountNbrencryptedValue = TestData.getCellData(sheetName, "accountNbrencryptedValue", row);
						String accountNbrdecryptedValue = TestData.getCellData(sheetName, "accountNbrdecryptedValue", row);
						String emailIdtagName = TestData.getCellData(sheetName, "emailIdtagName", row);
						String emailIdtagValue = TestData.getCellData(sheetName, "emailIdtagValue", row);
						String emailIdencryptedValue = TestData.getCellData(sheetName, "emailIdencryptedValue", row);
						String emailIddecryptedValue = TestData.getCellData(sheetName, "emailIddecryptedValue", row);
						String netMonthlyIncometagName = TestData.getCellData(sheetName, "netMonthlyIncometagName", row);
						String netMonthlyIncometagValue = TestData.getCellData(sheetName, "netMonthlyIncometagValue", row);
						String netMonthlyIncomeencryptedValue = TestData.getCellData(sheetName, "netMonthlyIncomeencryptedValue", row);
						String netMonthlyIncomedecryptedValue = TestData.getCellData(sheetName, "netMonthlyIncomedecryptedValue", row);
						String availableLimittagName = TestData.getCellData(sheetName, "availableLimittagName", row);
						String availableLimittagValue = TestData.getCellData(sheetName, "availableLimittagValue", row);
						String availableLimitencryptedValue = TestData.getCellData(sheetName, "availableLimitencryptedValue", row);
						String availableLimitdecryptedValue = TestData.getCellData(sheetName, "availableLimitdecryptedValue", row);
							
		String b="{\"requestTrackCode\":\""+requestTrackCode+"\",\"tags\":[{\"tagName\":\""+vCardCustomerCodetagName+"\",\"tagValue\":\""+vCardCustomerCodetagValue+"\",\"encryptedValue\":\""+vCardCustomerCodeVencryptedValue+"\",\"decryptedValue\":\""+vCardCustomerCodedecryptedValue+"\"},{\"tagName\":\""+lenderAppNumbertagName+"\",\"tagValue\":\""+lenderAppNumbertagValue+"\",\"encryptedValue\":\""+lenderAppNumberencryptedValue+"\",\"decryptedValue\":\""+lenderAppNumberdecryptedValue+"\"},{\"tagName\":\""+lenderAccountNumbertagName+"\",\"tagValue\":\""+lenderAccountNumbertagValue+"\",\"encryptedValue\":\""+lenderAccountNumberencryptedValue+"\",\"decryptedValue\":\""+lenderAccountNumberdecryptedValue+"\"},{\"tagName\":\""+lenderRelationshipNumbertagName+"\",\"tagValue\":\""+lenderRelationshipNumbertagValue+"\",\"encryptedValue\":\""+lenderRelationshipNumberencryptedValue+"\",\"decryptedValue\":\""+lenderRelationshipNumberdecryptedValue+"\"},{\"tagName\":\""+dateOfBirthtagName+"\",\"tagValue\":\""+dateOfBirthtagValue+"\",\"encryptedValue\":\""+dateOfBirthencryptedValue+"\",\"decryptedValue\":\""+dateOfBirthdecryptedValue+"\"},{\"tagName\":\""+phoneNbrtagName+"\",\"tagValue\":\""+phoneNbrtagValue+"\",\"encryptedValue\":\""+phoneNbrencryptedValue+"\",\"decryptedValue\":\""+phoneNbrdecryptedValue+"\"},{\"tagName\":\""+idNumbertagName+"\",\"tagValue\":\""+idNumbertagValue+"\",\"encryptedValue\":\""+idNumberencryptedValue+"\",\"decryptedValue\":\""+idNumberdecryptedValue+"\"},{\"tagName\":\""+ifscCodetagName+"\",\"tagValue\":\""+ifscCodetagValue+"\",\"encryptedValue\":\""+ifscCodeencryptedValue+"\",\"decryptedValue\":\""+ifscCodedecryptedValue+"\"},{\"tagName\":\""+accountNbrtagName+"\",\"tagValue\":\""+accountNbrtagValue+"\",\"encryptedValue\":\""+accountNbrencryptedValue+"\",\"decryptedValue\":\""+accountNbrdecryptedValue+"\"},{\"tagName\":\""+emailIdtagName+"\",\"tagValue\":\""+emailIdtagValue+"\",\"encryptedValue\":\""+emailIdencryptedValue+"\",\"decryptedValue\":\""+emailIddecryptedValue+"\"},{\"tagName\":\""+netMonthlyIncometagName+"\",\"tagValue\":\""+netMonthlyIncometagValue+"\",\"encryptedValue\":\""+netMonthlyIncomeencryptedValue+"\",\"decryptedValue\":\""+netMonthlyIncomedecryptedValue+"\"},{\"tagName\":\""+availableLimittagName+"\",\"tagValue\":\""+availableLimittagValue+"\",\"encryptedValue\":\""+availableLimitencryptedValue+"\",\"decryptedValue\":\""+availableLimitdecryptedValue+"\"}]}";
/*
						String b = "{\n    \"requestTrackCode\": \"20210111T102318S2313091\",\n    \"tags\": [\n        {\n            \"tagName\": \"vCardCustomerCode\",\n            \"tagValue\": \"5021\",\n            \"encryptedValue\": \"OXOk+ylCOi/WTg0QOyGOqZCdKmos1ShAFIz9vg1zb8OqRhjxBXFwiT3ylA11s4nMdsjhx5kHctrgm2ySYP6vBtUvxI7fkjyh8OZryF0N0SbLUrXvPfQ9nGoj2uQUht3uYqUX6Njxked6l6RMPz68D4SroTol+jcV8NkPSSFu4vM3a799NiD7f+TDYWMZfnb/s8edCGsweW9CpGmPvreY9LofPzPoeMzgWrLXzzT+udMF1N6inqx3JFe40P2FmGwaotI6LqIIyyCvr+F3Lw5fO1OyEGeQwWxUZGGxsj/3Fwo2EjC9vsgcNfZ0eAYwpi8qMlTW6ZE8f9QC4vM22wcYIA==\",\n            \"decryptedValue\": \"705526543217658908760\"\n        },\n        {\n            \"tagName\": \"lenderAppNumber\",\n            \"tagValue\": \"5021\",\n            \"encryptedValue\": \"zgx1aQOhmptj8macCgoEcLTdauVD1qQNH91xAHEkd48Rp4fTLT+bMaoscRLWSLbVvdkZEh+XXnbXqtoT1yEplndG7cUKFCbMZtGe6IL/c8fZM4BC+mfxnDvicXAecshSuW46h2Pkwm9dwFTMFwPI5Nt82a+4b68l/v+gDIk3yUGhwBT8CEr+ZTQq3DzmWxZPQHfD/iEErN261BzymmHURpBZledOViuyfsUD1NGWPLxmp65nL8xXOEXMlFrmhySKJhZXdLJyjtWHKEkWw+lsOSFnEKCjIO9HL3/3I0Y/u7srHeCVZAqyWhgJvpejAkBEVKQp46nYpOu29w1/JzdLtQ==\",\n            \"decryptedValue\": \"704146749353453598374534539873453459387353958376485354986734598763458375354398763543587886879865432134567876567890123212344568420\"\n        },\n        {\n            \"tagName\": \"lenderAccountNumber\",\n            \"tagValue\": \"5021\",\n            \"encryptedValue\": \"uU5BZP8fz5tqNxNlspXk5pIAkV6nqPGOwPe6CsmhrXgmZPyfRme7gFr+eJJLvfqLlHqRo/fLn8v7Ky5ArqLQd8he/X+AzqtV3HDnvQRv3f7m5onEWsF01j7j9W2mVyxrj0M2eRibZO7ita3V0HZzp/z3btbFoW6bPRRywfDBQGVgFuHYc3eEXG4QCDUfCNtERtooA56LsUppuude8FPr5xO6ftlMrHvsszAVU2c1/YVAKGwNir1chPYO77DCUYL9aj5O2qZ6yqoGH5TaNv7MEtC/oJNB7z+K5g5YPvpHNdj+vPfEfpKsF2TzkXooiNa7uhEOlmuyD9Ov+vdrWpvHLg==\",\n            \"decryptedValue\": \"715265467890765432123456543289087654321234543567861\"\n        },\n        {\n            \"tagName\": \"lenderRelationshipNumber\",\n            \"tagValue\": \"5021\",\n            \"encryptedValue\": \"MMG4BWZ6VprEp8iYZkVNf0C31Ed5EuTh2193VnaJvr7Ii0qcW+AJL57OSavU9bvZlCL/VWbSLZBsOfio8I9LQXnVNCCKdQDpO1HqBP/+LkAP2jH0lEVv9imMDDOSgY51fpWmxutkzWiAuI0CTZN6vOicWUEmJ6E0gSRB6ZXEu/p/0rnKQyVzR9+U7sPiPEsI77ZDIo1jA3lgbu0fF1QdFSntGPiKMqWstSFZpj6Oj7ZuceUqiWfEjrRY28UUK64DFyYvuB9S/vdG3XMp+elfIXAfwdA+8saTuMG/D5ERgGf+HhL48kAq7Uf6EKIy9v7sn7hc1i6ucjW99GDzvWg7pw==\",\n            \"decryptedValue\": \"7152543212343245678965432123456789098765432123456789098765432123456789012345678909876543212345678909876543212345678909876543212345678909876543212345678\"\n        },\n        {\n            \"tagName\": \"dateOfBirth\",\n            \"tagValue\": \"04/04/1991\",\n            \"encryptedValue\": \"cq9O8vbIxeDR5/t2EwZ5Bbd2R9iNiyizGP1rtr9g4D2JaOsVWGbmEQ81bZDUSECChuuOJKvn31bzezs8q/F7kCM65O0cRHrrVcgdlnlBCxJF1tzUvK5VTcGbYJSDObhqimIYSCRjHn9DOaPfA14vmNS0FrjZeg38l5Lgjmy4zHwZ0r46+5IhZ1Vz9gniNYh0pnUIRsSqH/wKeQFWXIwl5De1+EkbflDw0aOzkBc7uhS+PAqS5wkbnKI1X6hBgDID+l/zQsmOfINY/jcqRP7CHhkgkmJblngrDL25OeGm+1kWbA/LPWaLWvcsZLdE0f9hW2QgqIL4xaMFymG9iPiSvA==\",\n            \"decryptedValue\": \"14/12/1991\"\n        },\n        {\n            \"tagName\": \"phoneNbr\",\n            \"tagValue\": \"8688795021\",\n            \"encryptedValue\": \"kUxxCcEW9qOdQXpG3cKbYJhwe4P5ndm/aBaCqtg41R6AJtZlagFsCXTgU7nu3FhDGQEWGOl0pz/XH6tBIzjOR2qLoQdlGQzVtJcLcDcaSIW1EKqemHME9FWn+RUa69bdZiwE09IR0ELOhYfk0fQz0bw1cVAY2HvgqLdwf3jxgH5L99mqH3q8N8yurqqM1vDBUUFVk/B5pnHDDH093/ucXSv8rI/o1GM4Jh0jOEfSum7YX5u8OIn7/kFZz1Oxxtd44XqrzDTNGwsSFCsdVyM9shZvrKNWCClXpoKGMuUjDy8iYiEetIRRRad4c3IJllFNi1nBe7/vRqF8vtdvCWWP9g==\",\n            \"decryptedValue\": \"98930082210\"\n        },\n        {\n            \"tagName\": \"idNumber\",\n            \"tagValue\": \"BPKPM5021J\",\n            \"encryptedValue\": \"Y/BSmT8fnWBcrd+kFuwUC8GHaG3KeHxhh7dQGdAN4rRkNiGmSAVDoWUEYYUrAWbWiuIjIidc0AWtDlrjtawPhb/hdLqXL159CZ/cqkhCok/LdGo+1k7xwz3SqP9sYMTa59pP0Tq/brSQeswYCt4jTTiuuu7Mx8RqX5uGlv3gBJmBujH4XFdz/y30pRMIJjMWzYwkHjcyr98OlEGGQdaSoPjIkYmzsn/0cf7VhbywmNLxlr6sx7c5WeynLqUrk0CoYk7BQZOa77+QioOzsUXn3PG2N+dMDNv0mweP2/WyxTm3tJKG0lQ37dpi5QD/qyjpb8zGh9eaItsyE/5W1D8szw==\",\n            \"decryptedValue\": \"634953JH5298765432120\"\n        },\n        {\n            \"tagName\": \"ifscCode\",\n            \"tagValue\": \"ABHY0065053\",\n            \"encryptedValue\": \"fOjwI2USWtpxJMtz2CWxGYTX/dp2URLeY2Tqwl0Uzo97k32gS8o6408P2wf92dpX+dg7hPQsjsF+LH+KLOw7n6PmrzpMiKntCdVW1qQ7ff/sGClm1ML2Gx9JUnZclIfYn/8WBXUYtDDGsSu+O0WaFVqRnVg0JU6j0+chvEyHVcTJ0toeGWsrvPOIQdV9MUm4fTBCJht1PRcZT+XJs8BgaZSmm5Aixbz3K6ffoFC0QbtY67XYD4w21liSSIKimzSmSo5uIiibIxNJvwEKAWac7yP9sQbcw83+RnTfWOpHo9zuppi573zhOShF+DTQxevS+6ewgIxsC//iXHQVV8dXPg==\",\n            \"decryptedValue\": \"ABHY0065053\"\n        },\n        {\n            \"tagName\": \"accountNbr\",\n            \"tagValue\": \"210310001025021\",\n            \"encryptedValue\": \"0nipBVAwHiAHhTVwQFWNPbneFaV7Z3n7mAVTByyjw4s9fZoKJZ3AYM/yzRiswOOmn7nj0HCD4QG8zmSQexBAqCZF+B4116AQg9WqDiwQCi0E4Z9MLyTuaGgPe/E1u1GnFnso/oHHsNXflPN3DZBIwLL8GcDOhcjhmPcoFlMXQiyf+khkzNu33oFDTzpZXbHNArwNgvrkrRCAH64bC+MlB8hGj6msmb9WkMpJ2KZPq9s8j7Su7mI8SqdZPE13pKRAIEITv6C8R6/vu9tczLAd69G0xyKPGq6qNN4kEpS7NRqedzGekVqKc/tTRal/HscUUKyLsjMT24xt4TelWQ3ZYA==\",\n            \"decryptedValue\": \"1134669\"\n        },\n        {\n            \"tagName\": \"emailId\",\n            \"tagValue\": \"rajesh.manthena@qfund.net\",\n            \"encryptedValue\": \"jWgm8TCr/yOuUk6isRq6RaVdGFwkGB4a0ISL3j9OcV7otlw8Z1czIoYnyAHqHciBNXjAeu6nhcD7/2hCV5c7BbszMqXFtrZVbwGnk1McyGMABnWed9mIoGCUTQq4z5dMNUGiQ3yauD2CDIngEj60sS45GorhwB4QQaAGzgTJP4v3QyD5Gu9qkdEAoKBnad8louF6AqV3SIQgEF4Ykm30gJ3ntIvpl4+pKXvPZMuFLMxXirUl3Z5m0RfVbg4dhzhlghNeVfNRUbehll9PoXgUB8B0ywbTmwLpxOUwqr8QqNpVNMbkES0+ygndEXt6nW0XDOsyRt1ftVKEXl6QhQ9jaQ==\",\n            \"decryptedValue\": \"pavannnnnnnnnnnnnnnnnnnnnnnnttttttttttttttttttttbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccc.a@qfund.net\"\n        },\n        {\n            \"tagName\": \"netMonthlyIncome\",\n            \"tagValue\": \"30000\",\n            \"encryptedValue\": \"hwf+XqvsR9jZQILIuf4sffK+hSadlU4LTRKNhqVBIWvILDULIzdpzS4eOe1K/VPr27Fk+7154Y7xGsnhyjVYVWcQFgkRHqi4AkiuzZ89Qx0qY7UB+bhLToWU7kNCcoF+2ohE1NVpAKELzx7MJCSjyiJf/cMe8cBNnOsuO3//Dpiz8B+faFyOgv3crM5zrtss6let5v4moFoErq6zzlrNO7CrsZp4iRwwDpduV3YygaTUrtb93h0vEvvx4xoP4xDrH5+n1OXwuBh5kGjnVr6DU7mQKXz849974Gzc/LO7r0gzolAjFS0Oi58VIdNOFQVcPbaghCVoPNT2ZDinBxPgnw==\",\n            \"decryptedValue\": \"60000000000\"\n        },\n        {\n            \"tagName\": \"availableLimit\",\n            \"tagValue\": \"30000\",\n            \"encryptedValue\": \"yLqCnlQa9NSPxpzv7KK7QEPGgNCmUdn9sfAIT9MDUgpPCLO2Oz3IkOP+sF4vBySqBduhW29w96An3Y7/xc+j7uZ+avESKPqt9pDuBs4yc2ZhcgebqgdPCJjQqDL16xy2jyzxrSXIWEQTutbmOexqW6LnzIvyHDXH6zQpy6GwEzbC5IPB6RINCpz4KGw5KkfRT2MOIfH7sMlURggAvga3HXVzDbKECblsNA6pCp7IN6l/S1AWODTACYd9KJoTf9E8FuSwifsDJA/xVbnfFEYLQcrnICbvAwNqO8ANyajdTajOiD1FMcNKma7EGHS9wFjPvxgG5VOeguEiZ8N7p+DmkA==\",\n            \"decryptedValue\": \"60000\"\n        }\n    ]\n}";

						
						*/
						

						
						
						Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}
	public static Response addCustomerInfo( int row) throws Exception{
		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
			

					
			
						String bearerToken = TestData.getCellData(sheetName, "Token", row);
						String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
						String lenderAppNumber = TestData.getCellData(sheetName, "lenderAppNumber", row);
						String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
						String lenderRelationshipNumber = TestData.getCellData(sheetName, "lenderRelationshipNumber", row);
						String vdf1 = TestData.getCellData(sheetName, "vdf1", row);
						String vdf2 = TestData.getCellData(sheetName, "vdf2", row);
						String vdf3 = TestData.getCellData(sheetName, "vdf3", row);
						String vdf4 = TestData.getCellData(sheetName, "vdf4", row);
						String vdf5 = TestData.getCellData(sheetName, "vdf5", row);
						String firstName = TestData.getCellData(sheetName, "firstName", row);
						String middleName = TestData.getCellData(sheetName, "middleName", row);
						String lastName = TestData.getCellData(sheetName, "lastName", row);
						String gender = TestData.getCellData(sheetName, "gender", row);
						String dateOfBirth = TestData.getCellData(sheetName, "dateOfBirth", row);
						String nationality = TestData.getCellData(sheetName, "nationality", row);
						String educationQualification = TestData.getCellData(sheetName, "educationQualification", row);
						String maritalStatus = TestData.getCellData(sheetName, "maritalStatus", row);
						String phoneNbr = TestData.getCellData(sheetName, "phoneNbr", row);
						String emailId = TestData.getCellData(sheetName, "emailId", row);
						String fatherName = TestData.getCellData(sheetName, "fatherName", row);
						String motherName = TestData.getCellData(sheetName, "motherName", row);
						String spouseName = TestData.getCellData(sheetName, "spouseName", row);
						String idProofType = TestData.getCellData(sheetName, "idProofType", row);
						String idNumber = TestData.getCellData(sheetName, "idNumber", row);
						String addressLine1 = TestData.getCellData(sheetName, "addressLine1", row);
						String addressLine2 = TestData.getCellData(sheetName, "addressLine2", row);
						String city = TestData.getCellData(sheetName, "city", row);
						String state = TestData.getCellData(sheetName, "state", row);
						String zipcode = TestData.getCellData(sheetName, "zipcode", row);
						String residenceType = TestData.getCellData(sheetName, "residenceType", row);
						String addressProofType = TestData.getCellData(sheetName, "addressProofType", row);
						String monthsAtResidence = TestData.getCellData(sheetName, "monthsAtResidence", row);
						String PaddressLine1 = TestData.getCellData(sheetName, "PaddressLine1", row);
						String PaddressLine2 = TestData.getCellData(sheetName, "PaddressLine2", row);
						String Pcity = TestData.getCellData(sheetName, "Pcity", row);
						String Pstate = TestData.getCellData(sheetName, "Pstate", row);
						String Pzipcode = TestData.getCellData(sheetName, "Pzipcode", row);
						String employerName = TestData.getCellData(sheetName, "employerName", row);
						String employerType = TestData.getCellData(sheetName, "employerType", row);
						String designation = TestData.getCellData(sheetName, "designation", row);
						String officeEmailId = TestData.getCellData(sheetName, "officeEmailId", row);
						String monthsAtCurrentJob = TestData.getCellData(sheetName, "monthsAtCurrentJob", row);
						String totalJobExperience = TestData.getCellData(sheetName, "totalJobExperience", row);
						String EaddressLine1 = TestData.getCellData(sheetName, "EaddressLine1", row);
						String EaddressLine2 = TestData.getCellData(sheetName, "EaddressLine2", row);
						String Ecity = TestData.getCellData(sheetName, "Ecity", row);
						String Estate = TestData.getCellData(sheetName, "Estate", row);
						String Ezipcode = TestData.getCellData(sheetName, "Ezipcode", row);
						String incomeType = TestData.getCellData(sheetName, "incomeType", row);
						String netMonthlyIncome = TestData.getCellData(sheetName, "netMonthlyIncome", row);
						String nextPayDate = TestData.getCellData(sheetName, "nextPayDate", row);
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																					
						
						
						
						
						
			String b="{\"partnerAccountDetails\":{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAppNumber\":\""+lenderAppNumber+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\",\"lenderRelationshipNumber\":\""+lenderRelationshipNumber+"\",\"vdf1\":\""+vdf1+"\",\"vdf2\":\""+vdf2+"\",\"vdf3\":\""+vdf3+"\",\"vdf4\":\""+vdf4+"\",\"vdf5\":\""+vdf5+"\"},\"customerInfo\":{\"customerCommonInfo\":{\"firstName\":\""+firstName+"\",\"middleName\":\""+middleName+"\",\"lastName\":\""+lastName+"\",\"gender\":\""+gender+"\",\"dateOfBirth\":\""+dateOfBirth+"\",\"nationality\":\""+nationality+"\",\"educationQualification\":\""+educationQualification+"\",\"maritalStatus\":\""+maritalStatus+"\",\"phoneNbr\":\""+phoneNbr+"\",\"emailId\":\""+emailId+"\",\"fatherName\":\""+fatherName+"\",\"motherName\":\""+motherName+"\",\"spouseName\":\""+spouseName+"\",\"idProofType\":\""+idProofType+"\",\"idNumber\":\""+idNumber+"\"},\"residenceAddressInfo\":{\"addressLine1\":\""+addressLine1+"\",\"addressLine2\":\""+addressLine2+"\",\"city\":\""+city+"\",\"state\":\""+state+"\",\"zipcode\":\""+zipcode+"\",\"residenceType\":\""+residenceType+"\",\"addressProofType\":\""+addressProofType+"\",\"monthsAtResidence\":\""+monthsAtResidence+"\"},\"permanentAddressInfo\":{\"addressLine1\":\""+PaddressLine1+"\",\"addressLine2\":\""+PaddressLine2+"\",\"city\":\""+Pcity+"\",\"state\":\""+Pstate+"\",\"zipcode\":\""+Pzipcode+"\"},\"employerInfo\":{\"employerName\":\""+employerName+"\",\"employerType\":\""+employerType+"\",\"designation\":\""+designation+"\",\"officeEmailId\":\""+officeEmailId+"\",\"monthsAtCurrentJob\":\""+monthsAtCurrentJob+"\",\"totalJobExperience\":\""+totalJobExperience+"\"},\"employerAddressInfo\":{\"addressLine1\":\""+EaddressLine1+"\",\"addressLine2\":\""+EaddressLine2+"\",\"city\":\""+Ecity+"\",\"state\":\""+Estate+"\",\"zipcode\":\""+Ezipcode+"\"},\"incomeInfo\":{\"incomeType\":\""+incomeType+"\",\"netMonthlyIncome\":\""+netMonthlyIncome+"\",\"nextPayDate\":\""+nextPayDate+"\"}}}";

					
					/*	
						String b = "{\n	\"partnerAccountDetails\": {\n		\"vCardCustomerCode\": \""+vCardCustomerCode+"\",\n		\"lenderAppNumber\": \""+lenderAppNumber+"\",\n		\"lenderAccountNumber\": \""+lenderAccountNumber+"\",\n		\"lenderRelationshipNumber\": \""+lenderRelationshipNumber+"\",\n		\"vdf1\": \""+vdf1+"\",\n		\"vdf2\": \"DEF\",\n		\"vdf3\": \"GHI\",\n		\"vdf4\": \"JKL\",\n		\"vdf5\": \"MNO\"\n	},\n	\"customerInfo\": {\n		\"customerCommonInfo\": {\n			\"firstName\": \"Rajesh\",\n			\"middleName\": \"kumar\",\n			\"lastName\": \"Manthena\",\n			\"gender\": \"MALE\",\n			\"dateOfBirth\": \"2GNWnl3ykTeXVkaFkULvgp6g0Yf3Wv6ls+NN/P6z5RobjIJAp7yHM57QT+z9zcQvfjichZH1P7AbUypGs6BmI+DC3wPawsTbyssjkXPtdsxcgMEUxRFiD8A2waF54F2ZLPh37deLvAwCb3mAMuRJd5qMu1MjcgwGWOQX1o3ZAx5jS9uYhc2x+fE+ukL6PgGEKcsZ7p0kgF07BNNrxxBYUZp9lhZCJC7Q4BsoItvXpaDo3aPVlI652c4idjs3wy7hC4NhqO5Fm9JNfxixaOc0+WoYs7tG7gO/7U0gYvkNnZ6EkY4+9XufLUpU8nDx7Wgjr2jBarJDRYrAmGII83MtAg==\",\n			\"nationality\": \"Indian\",\n			\"educationQualification\": \"3\",\n			\"maritalStatus\": \"1\",\n			\"phoneNbr\": \"ALiogrgTuCeBkgktFOCRtoqcpDmGMCMWTYWtfsxV1w/9QIoWm7ty/Lf9KlRjVlc+pxS2k/Xobnf1Sk/0fzZRIIo8e92DOdoVZUm/XmVgMTAPhvtUWxm6aw+/vff+QHdbh0BJtt2VD0KtGEWFzcBlzOUXPahiIMUvv6p9Hqql6hRjihOzm78GbjbqMRSrf9Xzqh+3/LF7f0t0gqSls91zdDHX93GaGAYeKDlo2tybU9XkEPP/rAAe8XYgaqSIgUhrdI33RY2la3LpC5DADyOKIF4t6iSsnWfBlJjXX2dKT+LtStfl16z0iS2CPsWH+7ncEs6OuCjksto2cK3XhBQpCQ==\",\n			\"emailId\": \"ti2+KunxxC13t8X8HQ0oGMVfA8AG0D9gltb0LdFRNKZjb/PhrRVcQvZSnEZwplS48UmDfwMph+0obD66a6Eyp4VTMjH/uPNS09Osxqw9k8Rhq46izskSMVKGWjFx7QxQkmnOyrt9wdgvq5tMD2viHCIhVWZ3LeUqhM+Ceu2MqVTVshvoM3DwEwJ6ebmueVg3T9NilRhD+cmwF3F285dlJ6SBgJpl+bn5+W7ND5CoPWoIcXYRG2gYknFtw60U0Gug6FE29hPdLVoWqIcIeRBEMVXr1jnfqvZPUomnkOFH5JGa+5mIhA0UZOk2oky+sZlJE2dAwCpVigPhowAYMuSnjw==\",\n			\"fatherName\": \" Ramulu\",\n			\"motherName\": \"Rajeshwari\",\n			\"spouseName\": \"Manasa\",\n			\"idProofType\": \"PAN\",\n			\"idNumber\": \"3qqs/NgQAAh7HBGUy1rMAQXR4x73pMUYdbrmLsYDRxHgRQlTpLwjJq//P+n/bCNToR2BMWGzJGqjAKJUTYKN9rOV9hSgn0DGDnCEI2ep4H2YWuaRq3s5lKAkkdWwJzhDsEAbmL1OyEbAWLyzZB8bJKvs1Q6rb8dkp2gQhEKHOD50xXn3ctutuTxftDoXmtOs0CjhMLhSmkPnqelBP6NhxHS8V7eJNncxTNM5q0dYDVjBfGoeoTN+fvXeoerTZ2pmWcluWo/UQV2gn51Nr2LgB1oo+T9bA+ybdwVxQO+I4/FM7bj8yAcOxohKcwcl/Jk6xDZcePQbJj4rIQUNAxehZg==\"\n		},\n		\"residenceAddressInfo\": {\n			\"addressLine1\": \"Plot no-107\",\n			\"addressLine2\": \"Jawahar colony\",\n			\"city\": \"Hyderabad\",\n			\"state\": \"TS\",\n			\"zipcode\": \"500033\",\n			\"residenceType\": \"NRI\",\n			\"addressProofType\": \"PAN\",\n			\"monthsAtResidence\": 10\n		},\n		\"permanentAddressInfo\": {\n			\"addressLine1\": \"Plot no-107\",\n			\"addressLine2\": \"Jawahar colony\",\n			\"city\": \"Hyderabad\",\n			\"state\": \"TS\",\n			\"zipcode\": \"500033\"\n		},\n		\"employerInfo\": {\n			\"employerName\": \"HMWSSB\",\n			\"employerType\": \"GOV\",\n			\"designation\": \"Emp\",\n			\"officeEmailId\": \"rajesh.manthena@qfund.net\",\n			\"monthsAtCurrentJob\": \"10\",\n			\"totalJobExperience\": \"3\"\n		},\n		\"employerAddressInfo\": {\n			\"addressLine1\": \"Survey no-83\",\n			\"addressLine2\": \"Hakimpet\",\n			\"city\": \"Hyderabad\",\n			\"state\": \"TS\",\n			\"zipcode\": \"500083\"\n		},\n		\"incomeInfo\": {\n			\"incomeType\": \"EM\",\n			\"netMonthlyIncome\": \"gTgZAj+yTlfjmgxrNnDoXiwMDvs88L6/+a27oIg2YbJEwiIUA13nWCbGBFcfHnNWQbYfkGOs73dBp5wRyxF/yXPUDnCUE1s6XEGj7eNM0LUudwncZFxnSle35pqxo49FnMrycJODOwNPpsbZf58BtfWyegmPL9V863r76KBRgb92iVba5l/Jas0HIGgYZJO4e2iCTh5manXw1ip1UnkxTNkBi/dCyQTWmDCO4uHx+iTR/WUA87TE69bhyY+c+5Qbo2eBi51U05a0I1TAcRU9QKPnJuKWuZ3U06uEmLcl2IZF5dc4yMfyinmyzWqaSDY2moEd2v3Fxzaj58Ejrre5GA==\",\n			\"nextPayDate\": \"30/06/2021\"\n		}\n	}\n}";
						
						*/
		
						
						Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(b).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}	
	}

