package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class GetLoanDashBoardDetails  extends vcard{
	
	public static Response getLoanDashBoardDetails( int row, String bearerToken ) throws Exception{

	String method= new Exception() .getStackTrace()[0].getMethodName();
	test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
	
	
		String sheetName=method;
		int lastrow=TestData.getLastRow(method);
				
		//String bearerToken = TestData.getCellData(sheetName, "Token", row);
		String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);			
		String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
					/*String b = "{\n   \"vCardCustomerCode\": \"Li5KMKU+gPJGI6+sv8CgVjC+RrQoAwpn7XwKXEB4t7ofjgMVobktQ8/VO3xRgi9XJHvsfp6mQnVp2VjVARjgBsuVym0mvGP0dDjf+2CPV9puY9maVcWxPI9CZULCt0GSUYXdV5HKgpypp1MdXCvDNkRQg3K80ABYgymUTywCsqKaJ5XaRr5DqsUMaB+V0oSujydMjK6xd9lFem689trrqR2u+Hf5Gp+vzdX5GJoK7xKkMoByPh7NPG74ryS6YV/vhUSr3//Q9ytnCRYvlE9siAbO3mGF5zFn3S6MYAXTaV74YkpHUXiY/hnVN+ZzmjOiPFBJ3l45HzsAjGsvL9iPxw==\",\n    \"lenderAccountNumber\": \"nDPu9yLoDNvoCZWQc688ohpmcmmrVzL9Z8OROPvvn1MmFNsLbSHSYyzj5EFDBb2HCo6AYif0WLT15PlSu6wlMxwK6MxeGR58FsqGVQsSsbQLbQQbIEHeHQ0e/qPd8s2E4WG/vEOh+xbF+CMUM75mNkiPjj1/lPu6NdghbRXQj+0/ct60nmHLUyM8phedVbKl/KVfKpCjnoaLPIqMochsTQ49TDYEv7C4kauC1LE9J8w/W5Uodykc+2vCJP99kZ5+KLVBnZ0VgPE+OR3XjLf/DMHe3UA1qYrpHo4J1AZjMteDO1/BF2sZFLftmAeGImJiHL8XElXO2BXhRADcCzLcHA==\"\n}";
					*/
					
		String payload="{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\"}";			
					
						Response res=
							given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
							  .when().body(payload).post(baseuri+method).then().extract().response();
					
					
					
		
			return res;

}
	public static void getLoanDashBoardDetails_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>getLoanDashBoardDetails_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>getLoanDashBoardDetails_validate FAIL</b>");
				
			Assert.assertTrue(false);
			
		}
	}
}
