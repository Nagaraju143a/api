package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EMandateCustomerRequest extends vcard{

	

	
	public static Response eMandateCustomerRequest( int row ,String bearerToken) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						
					//	String bearerToken = TestData.getCellData(sheetName, "Token", row);
					//	String vc = TestData.getCellData(sheetName, "vCardCustomerCode", row);
					//	String ln = TestData.getCellData(sheetName, "loanNumber", row);
					//	String an = TestData.getCellData(sheetName, "accountNumber", row);
						
						String rd = TestData.getCellData(sheetName, "requestDate", row);
						
						String ammount = TestData.getCellData(sheetName, "requestAmount", row);
						
						int i =Integer.parseInt(ammount);
					
						String vc =	vCardCustomerCode;	
						String an =AccountNumberTAG;
						String ln =String.valueOf(loannumber);					
						// //loan orginate
						//String ln="1176";
	String payload="{\"vCardCustomerCode\":\""+vc+"\",\"loanNumber\":\""+ln+"\",\"accountNumber\":\""+an+"\",\"requestDate\":\""+rd+"\",\"requestAmount\":"+i+"}";					
				
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}

	
	
	
	
	public static void eMandateCustomerRequest_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

	//	test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			
			 JsonPath newData = res4.jsonPath();
			 	String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
				 
				 String status = newData.get("status");
				 test.log(LogStatus.INFO, "RESPONSE of status: "+status);
				 
				 
			 }else{
				 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>eMandateCustomerRequest_validate FAIL DUE TO</b>"+message);
					 
			 }
			
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>eMandateCustomerRequest_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>eMandateCustomerRequest_validate FAIL DUE TO</b>"+i);
				
			Assert.assertTrue(false);
			
		}
		test.log(LogStatus.INFO, "******************************************************** ");
	}
}
