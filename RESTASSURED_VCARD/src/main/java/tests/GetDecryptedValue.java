package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class GetDecryptedValue extends vcard{
	public static Response getDecryptedValue( int row ,String bearerToken) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			
						
						//String bearerToken = TestData.getCellData(sheetName, "Token", row);
					//	String vcd = TestData.getCellData(sheetName, "Token", row);
						//String tagd = TestData.getCellData(sheetName, "Token", row);
						
			String vcd = "vCardCustomerCode";
			String tagd = vCardCustomerCode;
						
	String payload="{\"tags\":[{\"tagName\":\""+vcd+"\",\"tagValue\":\""+tagd+"\"}]}";					
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						

							test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********body  API ********"+payload);			
			
				return res;
	
	}

	public static void getDecryptedValue_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>getDecryptedValue_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>getDecryptedValue_validate FAIL</b>");
				
			Assert.assertTrue(false);
			
		}
	}
	

}
